package gerador.numeros.aleatorios;

import java.util.Scanner;

public class GeradorNumerosAleatorios {

    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        double valorInicial;
        double valorFinal;
        
        System.out.print("1 - Uniforme\n2 - Triangular\n3 - Exponencial\n4 - Normal\nInforme a distribuicao desejada: ");
        int distribuicao = ler.nextInt();
        
        System.out.println("Informe a quantidade de números desejados: ");
        int quantidadeNumeros = ler.nextInt();
        
        switch (distribuicao) {
            case 1:
                System.out.println("Informe o valor inicial do intervalo: ");
                valorInicial = ler.nextDouble();
                System.out.println("Informe o valor final do intervalo: ");
                valorFinal = ler.nextDouble();
                
                System.out.println("Números Aleatórios:");
                for (int i=0; i<quantidadeNumeros; i++) {
                    System.out.println(valorInicial + (valorFinal - valorInicial) * Math.random());
                }
                break;
            case 2:
                System.out.println("Informe o valor inicial do intervalo: ");
                valorInicial = ler.nextDouble();
                System.out.println("Informe o valor final do intervalo: ");
                valorFinal = ler.nextDouble();
                System.out.println("Informe o valor da moda: ");
                double valorModa = ler.nextDouble();
                
                System.out.println("Números Aleatórios:");
                for (int i=0; i<quantidadeNumeros; i++) {
                    double aleatorio = Math.random();
                    if (((valorModa - valorInicial)/(valorFinal - valorInicial)) > aleatorio) {
                        System.out.println(valorInicial + Math.sqrt(aleatorio*
                                (valorModa - valorInicial)*(valorFinal - valorInicial)));
                    } else {
                        System.out.println(valorFinal - Math.sqrt((1 - aleatorio)
                                *(valorFinal - valorModa)*(valorFinal - valorInicial)));
                    }
                }
                break;
            case 3:
                System.out.println("Informe a média: ");
                double valorMedia = ler.nextDouble();
                
                System.out.println("Números Aleatórios:");
                for (int i=0; i<quantidadeNumeros; i++) {
                    System.out.println(-valorMedia * Math.log(Math.random()));
                }
                break;
            case 4:
                System.out.println("Informe o valor da média: ");
                double media = ler.nextDouble();
                System.out.println("Informe o valor da variância: ");
                double variancia = ler.nextDouble();
                
                System.out.println("Números Aleatórios Gerados:");
                for (int i=0; i<quantidadeNumeros; i++) {
                    double z = Math.sqrt(-2*Math.log(Math.random())) * Math.sin(2*Math.PI*Math.random());
                    System.out.println(media + variancia*z);
                }
                break;
        }
    }
}